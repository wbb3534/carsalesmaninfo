package com.seop.carcustomerinfo.service;

import com.seop.carcustomerinfo.entity.CustomerInfo;
import com.seop.carcustomerinfo.exception.CMissingDataException;
import com.seop.carcustomerinfo.model.CustomerItem;
import com.seop.carcustomerinfo.model.CustomerRequest;
import com.seop.carcustomerinfo.model.CustomerResponse;
import com.seop.carcustomerinfo.model.ListResult;
import com.seop.carcustomerinfo.repository.CustomerInfoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerInfoRepository customerInfoRepository;

    public void setCustomerInfo(CustomerRequest request) {
        CustomerInfo addData = new CustomerInfo.CustomerInfoBuilder(request).build();

        customerInfoRepository.save(addData);
    }

    public ListResult<CustomerItem> getCustomersInfo() {
        List<CustomerInfo> originData = customerInfoRepository.findAll();

        List<CustomerItem> result = new LinkedList<>();

        originData.forEach(item -> result.add(new CustomerItem.CustomerItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }
    public CustomerResponse getCustomerInfo(long id) {
        CustomerInfo originData = customerInfoRepository.findById(id).orElseThrow(CMissingDataException::new);

        return new CustomerResponse.CustomerResponseBuilder(originData).build();
    }
}
