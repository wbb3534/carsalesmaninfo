package com.seop.carcustomerinfo.controller;

import com.seop.carcustomerinfo.entity.CustomerInfo;
import com.seop.carcustomerinfo.model.*;
import com.seop.carcustomerinfo.service.CustomerService;
import com.seop.carcustomerinfo.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "고객관리")
@RequestMapping("/v1/customer")
@RestController
@RequiredArgsConstructor
public class CustomerController {
    private final CustomerService customerService;
    @ApiOperation(value = "고객정보 등록")
    @PostMapping("/new")
    public CommonResult setCustomerInfo(@RequestBody @Valid CustomerRequest request) {
        customerService.setCustomerInfo(request);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "고객정보 리스트")
    @GetMapping("/list")
    public ListResult<CustomerItem> getCustomersInfo() {
        return ResponseService.getListResult(customerService.getCustomersInfo(), true);
    }
    @ApiOperation(value = "고객정보 단수")
    @GetMapping("/data/{id}")
    public SingleResult<CustomerResponse> getCustomerInfo(@PathVariable long id) {
        return ResponseService.getSingleResult(customerService.getCustomerInfo(id));
    }
}
