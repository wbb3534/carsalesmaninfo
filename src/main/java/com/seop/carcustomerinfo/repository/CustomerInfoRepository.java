package com.seop.carcustomerinfo.repository;

import com.seop.carcustomerinfo.entity.CustomerInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerInfoRepository extends JpaRepository<CustomerInfo, Long> {
}
