package com.seop.carcustomerinfo.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
