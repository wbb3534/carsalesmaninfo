package com.seop.carcustomerinfo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarCustomerInfoApplication {

    public static void main(String[] args) {
        SpringApplication.run(CarCustomerInfoApplication.class, args);
    }

}
